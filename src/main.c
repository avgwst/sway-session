#include "commands.h"
#include <getopt.h>
#include <stdint.h>

#include <sys/stat.h>
#include <sys/types.h>

int custom_path = false;

char* json_default = "session.json";
char session_dir[MAX_PATH_LEN];

static struct option longopts[] = {
  { "file",    required_argument, NULL, 'f' },
  { "help",    no_argument,       NULL, 'h' },
  { "retries", required_argument, NULL, 'r' },
  { "verbose", no_argument,       NULL, 'v' }
};

/* Prints the help screen */
static void
usage(void)
{
  fprintf(stderr, "Usage: sway-session [-h] [-v] [-f <path to json file>] [command]\n\n"
      "Options:\n"
      "  --file     -f       Specify path to json, to save into or restore from\n"
      "  --help     -h       Shows this help screen\n"
      "  --retries  -r  {n}  Exit after N failed times (applies only to save-loop)\n"
      "  --verbose  -v       Be more verbose\n"
      "Commands:\n"
      "  save            Save current session\n"
      "  save-loop  {s}  Constantly save your session every N seconds\n"
      "  restore         Restore last session\n\n");
}

/* Check If the default directory exists, otherwise - create it */
static int
check_defaults(void)
{
  int status;
  struct stat st = {0};

  if (stat(session_dir, &st) == ERR) {
    fprintf(stderr, "Warning: default session directory does not exist\n");
    printf("Creating directory..\n");

    if ((status = mkdir(session_dir, 0777)) == ERR) {
      perror("Fatal: failed to create directory");
      return (status);
    }
  }

  return (EXIT_SUCCESS);
}

/* Enable auto mode, when no arguments provided */
int
auto_mode(const char* path)
{
  /* Try to restore previous session */
  fprintf(stdout, "Trying to restore existing session..\n");

  if (restore(path) == ERR)
    fprintf(stderr, "Warning: failed to restore session\n");

  for (;;)
    save_loop(path, SAVE_FREQ);

  return (0);
}

/* Kill other sway-session instances */
int
kill_instances(void)
{
  int i, k = 0, pids[5];
  char* buf = malloc(sizeof(char) * 50);
  FILE* instances;

  instances = popen("pgrep sway-session", "r");
  fgets(buf, sizeof(buf), instances);
  pclose(instances);

  for (pids[k] = i = 0; buf[i] != '\0'; i++) {
    if (isdigit(buf[i]) != 0) {
      pids[k] += buf[i] - '0';
      pids[k] *= 10;
    }

    if (buf[i] == '\n') {
      pids[k] /= 10;
      k++;
    }
  }
  pids[k] = '\0';
  free(buf);

  if (verbose == true) {
    for (i = 0; getpid() != pids[i] && pids[i] != '\0'; i++) {
      puts("------------------------");
      printf("Running instances:");
      printf(" %d", pids[i]);
      putchar('\n');
    }
  }

  for (i = 0; getpid() != pids[i] && pids[i] != '\0'; i++) {
    fprintf(stdout, "Killing running instance %d\n", pids[i]);
    if (kill(pids[i], SIGKILL) != 0) {
      fprintf(stderr, "Warning: failed to kill process");
    }
    puts("------------------------");
  }

  return (0);
}

int
main(int argc, char** argv)
{
  int ch, i, freq;
  const char *cmd, *file_path;
  FILE* session;
  home = getenv("HOME");

  while ((ch = getopt_long(argc, argv, "f:hr:v", longopts, NULL)) != -1) {
    switch (ch) {
      case 'f':
        custom_path = true;
        file_path = optarg;
        break;
      case 'h':
        usage();
        return (EXIT_SUCCESS);
      case 'r':
        err_limit = atoi(optarg);
        if (err_limit == 0) {
          fprintf(stderr, "Fatal: -r requires a number or zero was given\n");
          return (ERR);
        } else if (err_limit <= 0) {
          fprintf(stderr, "Fatal: number must be positive\n");
          return (ERR);
        }
        break;
      case 'v':
        verbose = true;
        break;
      default:
        return (ERR);
    }
  }
  cmd = argv[optind];

  /* Prechecks */
  if (strcmp(getenv("USER"), "root") == 0) {
    fprintf(stderr, "Fatal: do not run this as root\n");
    return (ERR);
  } else if (getenv("SWAYSOCK") == NULL) {
    fprintf(stderr, "Fatal: sway is not running?\n");
    return (ERR);
  }

  kill_instances();

  if (custom_path == false) {
    if (home != NULL) {
      char* session_dir_temp = (char*)calloc(25, sizeof(char));
      strcat(session_dir_temp, ".config/sway/session");

      /* Creating full path */
      strcat(session_dir, home);
      strcat(session_dir, "/");
      strcat(session_dir, session_dir_temp);

      if (verbose == true)
        printf("Session directory: %s\n", session_dir);

      free(session_dir_temp);

      if (check_defaults() != 0) {
        fprintf(stderr, "Fatal: failed to check defaults\n");
        return (EXIT_FAILURE);

      } else {
        if (chdir(session_dir) == -1) {
          fprintf(stderr, "Fatal: failed to change directory to %s\n", session_dir);
          return (EXIT_FAILURE);
        }
        file_path = json_default;
      }
    }
  }

  if (cmd == NULL) {
    fprintf(stderr, "Warning: no arguments provided\n");
    auto_mode(file_path);

  } else if (strcmp(cmd, "save") == 0) {
    if (save(file_path) == ERR)
      return (EXIT_FAILURE);

  } else if (strcmp(cmd, "save-loop") == 0) {
    char* sl_arg = argv[optind + 1];

    for (i = 0; isdigit(sl_arg[i]) != 0 && sl_arg[i] != '\0'; i++);

    if (i == strlen(sl_arg))
      freq = atoi(sl_arg);

    save_loop(file_path, freq);

  } else if (strcmp(cmd, "restore") == 0) {
    if (restore(file_path) == ERR)
      return (EXIT_FAILURE);

  } else {
    fprintf(stderr, "sway-session: unknown command -- %s\n", cmd);
    return (EXIT_FAILURE);
  }

  return (EXIT_SUCCESS);
}
