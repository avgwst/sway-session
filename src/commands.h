#include <ctype.h>
#include <dirent.h>
#include <json.h>
#include <signal.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>
#include <string.h>
#include <sys/wait.h>
#include <unistd.h>

#ifdef __FreeBSD__
  #include <sys/fcntl.h>
  #include <sys/errno.h>
#endif

#ifdef __linux__
  #include <fcntl.h>
  #include <errno.h>
#endif

/* FIXME: not really sure, how much memory should we allocate */
#define BUF_SIZE 65536
#define GET_TREE_CMD "swaymsg -t get_tree"
#define SAVE_FREQ 20

#define MAX_CMD_LEN 30
#define MAX_CLASS_LEN 30
#define MAX_OUTPUT_LEN 10
#define MAX_PATH_LEN 50

#define ERR -1

extern int err_limit;
extern int monn;
extern int verbose;
extern char* home;

struct current_props {
  char* app_id;
  char* class;
  char* output;
  int wrkspace;
};

int save(const char*);
int save_loop(const char*, int);

int restore(const char*);

void get_output(struct json_object*);
void get_class(struct json_object*);
int get_real_cmd(char[MAX_CLASS_LEN], char[MAX_CMD_LEN]);

void format_cmd(char [MAX_CMD_LEN]);

static int exec_prog(char*);
static int allign_window(void);
static int allign_workspace(void);
