#include "commands.h"

/* stop trying to save session after N attempts (5 is default) */
int err_limit = 10;
/* TODO: Return to the workspace, where the program was started initially */
/* int origin_workspace = 0; */
/* monitor number */
int monn = 1;
int verbose = false;
char* home;
struct current_props curr_prop;

/* Save current session */
int
save(const char* path)
{
  char tree[BUF_SIZE];
  FILE *pipe, *json;
  size_t to_read;

  json = fopen(path, "w");
  pipe = popen(GET_TREE_CMD, "r");

  while ((to_read = fread(tree, 1, sizeof(tree), pipe)) > 0)
    fwrite(tree, 1, to_read, json);

  if (verbose == true) {
    printf("Tree: %s", tree);
    printf("Tree length: %ld\n", strlen(tree));
  }

  if (strlen(tree) == 0) {
    fprintf(stderr, "Warning: session is NOT saved\n");
    /* Clean up the file */
    fprintf(json, "");
  } else {
    printf("Session saved\n");
  }

  pclose(pipe);
  fclose(json);
  return (EXIT_SUCCESS);
}

/* Save session every N seconds */
int
save_loop(const char* path, int freq)
{
  if (freq == 0)
    freq = SAVE_FREQ;

  for (;; sleep(freq)) {
    if (save(path) == ERR) {
      err_limit--;
      fprintf(stderr, "Warning: failed to save session\n");
      if (err_limit > 0)
        fprintf(stderr, "%d times left before exit\n", err_limit);
    }

    if (err_limit == 0) {
      printf("Fatal: to many errors, exiting\n");
      exit(1);
    }
  }
}

/* Restore current session */
int
restore(const char* path)
{
  char buf[BUF_SIZE];
  FILE* session_file = fopen(path, "r");
  enum json_tokener_error err;
  struct json_object *json, *root;

  if (session_file == NULL) {
    fprintf(stderr, "Fatal: failed to open session file\n");
    return (ERR);
  }

  fread(buf, 1, sizeof(buf), session_file);
  fclose(session_file);

  if (verbose == true)
    json = json_tokener_parse_verbose(buf, &err);
  else
    json = json_tokener_parse(buf);

  if (json == NULL) {
    if (verbose == true)
      fprintf(stderr, "Fatal: failed to parse JSON file: %s\n", json_tokener_error_desc(err));
    else
      fprintf(stderr, "Fatal: failed to parse JSON file\n");
    return (ERR);
  }

  root = json_object_object_get(json, "nodes");
  size_t len = json_object_array_length(root);

  if (root == NULL)
    return (ERR);

  for (size_t i = 0; i < len; i++) {
    get_output(json_object_array_get_idx(root, i));
    get_class(json_object_array_get_idx(root, i));
  }

  fprintf(stdout, "Session restored\n");
  return (EXIT_SUCCESS);
}

void
get_output(struct json_object* obj)
{
  const char* output = json_object_get_string(json_object_object_get(obj, "name"));
  curr_prop.output = malloc(sizeof(char) * MAX_OUTPUT_LEN);

  if (strncmp(output, "__i3", 4) == 0)
    return;

  if (output != NULL) {
    strncpy(curr_prop.output, output, strlen(output) + 1);

    if (verbose == true)
      printf("Monitor %d: %s\n", monn++, curr_prop.output);
  }
}

void
get_class(struct json_object* nodes)
{
  const char* shell = json_object_get_string(json_object_object_get(nodes, "shell"));
  int workspace = json_object_get_int(json_object_object_get(nodes, "name"));

  if (workspace > 0 && workspace <= 10) {
    curr_prop.wrkspace = workspace;
  }

  if (shell != NULL) {
    char cmd[MAX_CMD_LEN];
    json_object* window_props_obj = json_object_object_get(nodes, "window_properties");
    const char* app_id = json_object_get_string(json_object_object_get(nodes, "app_id"));
    const char* class = json_object_get_string(json_object_object_get(window_props_obj, "class"));

    if (app_id != NULL) {
      curr_prop.app_id = malloc(sizeof(char) * MAX_CLASS_LEN);
      strncpy(curr_prop.app_id, app_id, strlen(app_id) + 1);

      if (verbose == true)
        printf("app_id: %s\n", app_id);

    } else {
      curr_prop.class = malloc(sizeof(char) * MAX_CLASS_LEN);
      strncpy(curr_prop.class, class, strlen(class) + 1);

      if (verbose == true)
        printf("class: %s\n", class);
    }

    format_cmd(cmd);

    if (exec_prog(cmd) != 0) {
      fprintf(stderr, "Warning: failed to execute \"%s\"\n", cmd);
      get_real_cmd(cmd, cmd);
      exec_prog(cmd);
    }

    sleep(1);

    if (verbose == true)
      printf("Workspace: %d\n", curr_prop.wrkspace);

    if (curr_prop.wrkspace > 1) {
      if (allign_window() == ERR)
        fprintf(stderr, "Warning: failed to allign window to workspace %d\n", curr_prop.wrkspace);
      else if (allign_workspace() == ERR)
        fprintf(stderr, "Warning: failed to allign workspace %d to output %s\n", curr_prop.wrkspace, curr_prop.output);
    }

    if (curr_prop.app_id != NULL)
      free(curr_prop.app_id);
    else
      free(curr_prop.class);
  }

  json_object* nodes_obj = json_object_object_get(nodes, "nodes");
  size_t len = json_object_array_length(nodes_obj);

  for (size_t i = 0; i < len; i++)
    get_class(json_object_array_get_idx(nodes_obj, i));
}

int
get_real_cmd(char app_id[MAX_CLASS_LEN], char dest[MAX_CMD_LEN])
{
  int i, len, k;
  char file[50];
  char full_path[70];
  const char* dir;
  const char* search_for = app_id;

  /* Find the file and get the full path to it */
  /* TODO: also look at ~/.local/share/applications, when nothing in $PREFIX/share/application was found */
  /* ------------------------------------------------ */
  #ifdef __FreeBSD__
  dir = "/usr/local/share/applications";
  #endif
  #ifdef __linux__
  dir = "/usr/share/applications";
  #endif

  DIR* dr = opendir(dir);
  struct dirent* dir_entry;

  if (dr == NULL) {
    fprintf(stderr, "Cannot open directory %s", dir);
    return (-1);
  }

  /* get length of the search_for */
  for (len = 0; search_for[len] != '\0'; len++);

  if (verbose == true)
    fprintf(stdout, "Searching for %s...\n", search_for);

  while ((dir_entry = readdir(dr)) != NULL) {
    for (i = k = 0; dir_entry->d_name[i] != '\0' && search_for[k] != '\0'; i++) {
      if (dir_entry->d_name[i] == search_for[k]) {
        k++;
      } else {
        k = 0;
      }

      if (k == len) {
        strcpy(file, dir_entry->d_name);
      }
    }
  }

  if (file != NULL) {
    strcpy(full_path, dir);
    strcat(full_path, "/");
    strcat(full_path, file);
  } else {
    fprintf(stderr, "Warning: file not found in $PREFIX/share/applications");
    return (1);
  }

  if (verbose == true) {
    printf("Needed file: %s\n", file);
    printf("Full path to the file: %s\n", full_path);
  }

  closedir(dr);
  /* ------------------------------------------------ */

  /* Open the .desktop file and search for real command */
  FILE* f = fopen(full_path, "r");
  if (f == NULL) {
    fprintf(stderr, "Failed to open the file\n");
    return (-1);
  }

  char buf[10000], real_cmd[40];
  const char* filter = "Exec=";
  size_t read_siz;

  while ((read_siz = fread(buf, 1, sizeof(buf), f)) > 0)
    fwrite(buf, 1, read_siz, f);

  for (len = 0; filter[len] != '\0'; len++);

  for (i = k = 0; buf[i] != '\0' && filter[k]; i++) {
    if (buf[i] == filter[k]) {
      k++;
    } else {
      k = 0;
    }

    if (k == len) {
      int d = 0;
      for (int j = i + 1; buf[j] != ' ' && buf[j] != '\n'; j++) {
        real_cmd[d++] = buf[j];
      }
      real_cmd[d] = '\0';
    }
  }

  if (real_cmd != NULL) {
    printf("Command: %s\n", real_cmd);
  } else {
    fprintf(stderr, "Warning: failed to get real command");
    return (-1);
  }

  strcpy(dest, real_cmd);
  fclose(f);
  return (0);
}

/*
 * Convert app_id or class to command, which will be executed
 */
void
format_cmd(char dest[MAX_CMD_LEN])
{
  if (curr_prop.app_id == NULL)
    strncpy(dest, curr_prop.class, MAX_CLASS_LEN);
  else
    strncpy(dest, curr_prop.app_id, MAX_CLASS_LEN);

  /* always convert to lower case */
  for (int i = 0; dest[i] != '\0'; i++)
    dest[i] = tolower(dest[i]);

  if (verbose == true)
    printf("Formatted command: %s\n", dest);
}

static int
exec_prog(char* cmd)
{
  int pid, wstatus;
  char* curr_path = malloc(sizeof(char) * 50);
  char focus_cmd[22 + MAX_OUTPUT_LEN] = "swaymsg workspace ";

  getcwd(curr_path, sizeof(curr_path));
  focus_cmd[strlen(focus_cmd)] = curr_prop.wrkspace + '0';

  /* Temporary change current directory to $HOME */
  chdir(home);
  /* Switch to the correct workspace at first */
  system(focus_cmd);

  pid = fork();
  if (pid == -1) {
    perror("fork");
  }

  if (pid == 0) {
    execl("/bin/sh", "sh", "-c", cmd, (char*)NULL);
  } else {
    sleep(1);
    int w = waitpid(pid, &wstatus, WNOHANG);

    if (w == -1) {
      perror("waitpid");
    } else {
      if (WEXITSTATUS(wstatus) == 127)
        return (ERR);
    }
  }

  chdir(curr_path);
  free(curr_path);
  return (0);
}

/* Move window to output */
static int
allign_window(void)
{
  char mv_win_cmd[36] = "swaymsg move window to workspace ";
  int i = strlen(mv_win_cmd);

  if (curr_prop.wrkspace == 10)
    strcat(mv_win_cmd, "10");
  else {
    mv_win_cmd[i++] = curr_prop.wrkspace + '0';
    mv_win_cmd[i] = '\0';
  }

  if (system(mv_win_cmd) != 0)
    return (ERR);

  if (verbose == true)
    printf("Executed: %s\n", mv_win_cmd);

  return (EXIT_SUCCESS);
}

/* Move workspace to output */
static int
allign_workspace(void)
{
  char mv_wrkspc_cmd[31 + MAX_OUTPUT_LEN] = "swaymsg move workspace to output ";
  strncat(mv_wrkspc_cmd, curr_prop.output, MAX_OUTPUT_LEN);

  if (system(mv_wrkspc_cmd) != EXIT_SUCCESS)
    return (ERR);

  if (verbose == true) {
    printf("Executed: %s\n", mv_wrkspc_cmd);
  }

  return (EXIT_SUCCESS);
}
