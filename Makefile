NAME = sway-session
PREFIX ?= /usr/local
CFLAGS = -g -Werror
LDFLAGS = -ljson-c

.if "${.MAKE.OS}" == "Linux"
CFLAGS += -I/usr/include/json-c
COMPILER = gcc
.elif "${.MAKE.OS}" == "FreeBSD"
CFLAGS += -I/usr/local/include/json-c
LDFLAGS += -L/usr/local/lib
COMPILER = cc
.else
.error "OS is not supported"
.endif

all: src/main.c
	$(COMPILER) $(CFLAGS) $(LDFLAGS) src/main.c src/commands.c -o $(NAME)

install:
	install $(NAME) $(PREFIX)/bin

deinstall:
	-rm $(PREFIX)/bin/$(NAME)
