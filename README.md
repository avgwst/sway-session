[![Please don't upload to GitHub](https://nogithub.codeberg.page/badge.svg)](https://nogithub.codeberg.page)
# sway-session

## Usage

To save the current session:  
```
sway-session save
```
A json file will be stored at **$HOME/.config/sway/session** by default.  

Restoring is simply as:  
```
sway-session restore
```

To save you session every 45 seconds, use:  
```
sway-session save-loop 45
```

You can also run sway-session without any arguments. It's very useful for using in config.  

You may put this line in your sway config:  
```
exec sway-session
```

For more details please run:  
```
sway-session -h
```

## Known bugs and limitations

- The program will incorrectly align your windows, if you focus/move to another workspace/window or move your cursor 
- Terminal session will not be saved, so if you're editing text in a terminal with vi\*, after restoring it will start just empty terminal  

## TODOs

Rewrite it, so it won't be so complicated :)

---
**The project was highly inspired by https://github.com/gumieri/sway-session, so please check this project too**

